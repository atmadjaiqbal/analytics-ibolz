var IbolzAdmin = IbolzAdmin || {};

IbolzAdmin.Login = (function($){
    var self = {
        init: function(){
            self.placeholder();
            self.bindSubmit();

            $('.validate_form').bind('error_validation', function(a,e){
            });

        },
        bindSubmit: function(){

            // global submit
            $('.btn-submit').click(function(){
               $(this).closest('form').submit();
               return false;
            });

            $('input').keypress(function(e){
                if(e.which == 13) {
                    $('form').submit();
                    e.preventDefault();
                }
            });
        },
        placeholder: function(){
            $('input[data-placeholder]').each(function(){
                $(this)
                    .focus(self.placeholderFocus)
                    .blur(self.placeholderBlur);
                $(this).trigger('blur');
            });
        },
        placeholderFocus: function(){
            var placeholder = $(this).data('placeholder');
            if($(this).val() == placeholder){
                $(this)
                    .val('')
                    .removeClass('placeholder');
            }
        },
        placeholderBlur: function(){
            var placeholder = $(this).data('placeholder');
            if($(this).val() === '' || $(this).val() == placeholder){
                $(this)
                    .val(placeholder)
                    .addClass('placeholder');
            }
        }
    };
    return self;
})(jQuery);

(function($) {
    if($('.login').length){
        IbolzAdmin.Login.init();
    }
})(jQuery);