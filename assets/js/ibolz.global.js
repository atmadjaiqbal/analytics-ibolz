var Ibolz = Ibolz || {};

Ibolz.Global = (function($){
    var _today;
    var self = {
        init: function(){
            self.setNavigation();

        },

    setNavigation: function(){
        $('#main-nav > ul > li')
            .hover(
            function(){ $(this).addClass('hover');},
            function(){ $(this).removeClass('hover');}
        )
            .each(function(){
                $(this).css('width',$(this).width());
            });
    }


    };

    return self;
})(jQuery);

(function($) {
    Ibolz.Global.init();
})(jQuery);