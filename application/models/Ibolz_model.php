<?php
class Ibolz_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function get_channels($channel_id=NULL)
    {
        $condition1 = !empty($channel_id) ? 'AND ac.`channel_id`="'.$channel_id.'"' : '';
        $sql = "
		SELECT x.channel_id, x.channel_name, y.count_mobile_id, y.count_client_id, duration FROM
		(
		SELECT DISTINCT ac.channel_id , ch.channel_name
		FROM  apps_channel ac
		INNER JOIN channel ch ON ac.channel_id = ch.channel_id
		WHERE ch.channel_type_id IN (1,2) ".$condition1."
		) AS `x`
		LEFT JOIN
		(
		SELECT channel_id, COUNT(DISTINCT mobile_id) AS count_mobile_id, COUNT(client_id) count_client_id ,
		SUM(TIMESTAMPDIFF(MINUTE, start_date, end_date)) duration
		FROM content_activity
		GROUP BY  channel_id
		) AS `y` ON x.channel_id = y.channel_id
		ORDER BY x.channel_name";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function graph_analytics($channel_id=null,$start=null,$end=null)
    {
        if($channel_id != null || $start != null || $end != null){
            $sql = "
    		SELECT channel_id, channel_name, SUM(mobile_count) nb_mobile_count, SUM(client_count) nb_client_count, 
				SUM(duration_count) nb_duration, DATE(start_date) start_date, DATE(end_date) end_date,day, 
				CONCAT(day,'-',MONTH(start_date),'-',YEAR(start_date)) showndate from(
				SELECT x.channel_id channel_id, x.channel_name channel_name, y.count_mobile_id mobile_count, 
				y.count_client_id client_count, duration duration_count,start_date,end_date,DAYOFMONTH(start_date) day FROM
				(
					SELECT DISTINCT ac.channel_id , ch.channel_name
					FROM  apps_channel ac
					INNER JOIN channel ch ON ac.channel_id = ch.channel_id
					WHERE 
					ch.channel_type_id IN (1,2)
					AND ac.`channel_id`='".$channel_id."'
					) AS `x`
					LEFT JOIN
					(
					SELECT channel_id, COUNT(DISTINCT mobile_id) AS count_mobile_id, COUNT(client_id) count_client_id ,
					SUM(TIMESTAMPDIFF(MINUTE, start_date, end_date)) duration,start_date,end_date
					FROM content_activity
					GROUP BY end_date
					) AS `y` ON x.channel_id = y.channel_id
					where DATE(y.start_date) >= '".$start."' AND DATE(y.start_date) <= '".$end."'
					ORDER BY y.start_date
				)subquery1 GROUP BY DATE(start_date)";
            $query = $this->db->query($sql);
            return $query->result();
        }else{
            return false;
        }
    }

}

/*
EOF
*/