<!DOCTYPE html>
<html lang="en" class="login_page">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>IBOLZ Admin - Login</title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/ibolz.analytics.css" />
    <!--[if lte IE 8]>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/html5.js"></script>
    <![endif]-->

    <!-- favicon -->
    <link rel="shortcut icon" href="favicon.ico" />

</head>
<body>
<div class="login">
    <header>
        <h1><a href="" class="title">Wellcome To Ibolz Admin</a></h1>
        <span class="pic"></span>
    </header>
