   </div>
  </div>

  <footer id="footer" style="margin-top:20px;">
    <p align="center"><strong>&copy; <?php echo date('Y'); ?> IBOLZ Indonesia</strong></p>
  </footer>

  </div>
</body>

<script type="text/javascript">
    <?php $setting = array(
        'base_url' => base_url()
    ); ?>
    var settings = <?php echo json_encode($setting); ?>;
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ibolz.global.js"></script>
   <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ibolz.login.js"></script>


<script type="text/javascript">
    $(document).ready(function() {

        $("#username").change(function() {

            var usrname = $(this).val();
            $.ajax({
                url: '<?php echo base_url(); ?>login/listgroups?accid=' + usrname,
                type: "GET",
                success: function(data) {
                    $('#groupchannel').empty();
                    $('#groupchannel').html(data);
                }
            });

        });

    })
</script>

</html>
