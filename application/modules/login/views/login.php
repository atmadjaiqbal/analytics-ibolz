<?php echo form_open('login/verify_login'); ?>
<fieldset>
    <p class="info">Please log in with your username and password.</p>
    <div class="notification notification-error"><?php echo validation_errors(); ?></div>
    <label for="username">Username</label>
    <input type="text" name="username" class="text" id="username" legend="Username" data-validation="{required:true}">
    <label for="password">Password</label>
    <input type="password" name="password" class="text" id="password" legend="Password" data-validation="{required:true}">
    <label for="password">Group</label>
    <select id="groupchannel" name="selectchannelgroup" class="text"></select>
    <div class="login-action">
        <a href="#" class="button btn-submit">Login</a>
    </div>
    <?php /*<input type="hidden" name="redirect" value="<?php echo $redirect; ?>"> */?>
</fieldset>
</form>
