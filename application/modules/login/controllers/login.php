<?php
class Login extends Application {

    var $login_session = '';

    public function __construct()
    {
        parent::__construct();
        $data = $this->data;
        // $this->current_url = '/'.$this->uri->uri_string();
        // $this->session->set_flashdata('referrer', $this->current_url);
        $this->load->library('login/account_lib');

    }

    function index()
    {
        $this->load->helper(array('form'));
        $data['heading'] = "Ibolz Analytics";
        $html['html']['content']  = $this->load->view('login', $data,TRUE);
        $html['html']['header']   = $this->load->view('login/login_header', $html,TRUE);
        $html['html']['footer']   = $this->load->view('login/login_footer', $html,TRUE);
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function verify_login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

        $user_id = $this->input->post('username');
        $password = $this->input->post('password');
        $groups = $this->input->post('selectchannelgroup');
        $result = $this->account_lib->auth($user_id, $password, $groups);
        if(!$result)
        {
            $data['heading'] = "Ibolz Analytics";
            $html['html']['content']  = $this->load->view('login', $data, TRUE);
            $html['html']['header']   = $this->load->view('login/login_header', $html, TRUE);
            $html['html']['footer']   = $this->load->view('login/login_footer', $html, TRUE);
            $this->load->view('template/tpl_one_column', $html, false);
        } else {
            $sess_array = array('account' => $result, 'username' => $result->display_name);
            $this->session->set_userdata('logged_in', $sess_array);
            redirect('home', 'refresh');
        }

        /*
        if($this->form_validation->run() == FALSE)
        {
            $data['heading'] = "IBOLZ INDONESIA";
            $html['html']['content']  = $this->load->view('login', $data, TRUE);
            $html['html']['header']   = $this->load->view('template/login_header', $html, TRUE);
            $html['html']['footer']   = $this->load->view('template/login_footer', $html, TRUE);
            $this->load->view('template/tpl_one_column', $html, false);
        } else {
            redirect('home', 'refresh');
        }
        */

    }

    public function check_database()
    {
        $user_id = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->account_lib->auth($user_id, $password);
        if($result)
        {
            $sess_array = array();
            foreach($result as $row)
            {
                $sess_array = array('account' => $result, 'username' => $row->display_name);
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return FALSE;
        }

    }

    function listgroups() {
        $username = $this->input->get('accid', true); $_opt = '';
        $resGroup = $this->account_lib->get_useraccountid($username);
        foreach($resGroup as $row)
        {
            $_opt .= '<option value="'.$row['groups_id'].'">'.$row['groups_name'].'</oprion>';
        }
        echo $_opt;
    }

    function logout() {
        // session_start();
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('home', 'refresh');
    }

    public function language(){
        $language = $this->input->get("language");
        if($language){
            $this->session->set_userdata('language', $language);
        }
    }
}
?>