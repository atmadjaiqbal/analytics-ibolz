<?php
class Account_Lib {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
    }

    public function auth($user_id, $password, $groups) {
        $password = md5('mat' . $password . 'kra');
        $textquery = "
			SELECT
				account_id, user_id, email, fname, lname,
				display_name, phone, birthdate, gender, thumbnail, active,
				last_login, account_type_id
			FROM account
			WHERE user_id = '$user_id'
			AND password = '$password'
			AND active = 1
		 	";
        $result = $this->CI->db->query($textquery)->row();

        $account_privilege = array();
        $account_role = array();
        if ($result) {
            $textquery = "UPDATE account SET last_login = now() WHERE account_id = '$result->account_id'";
            $this->CI->db->query($textquery);

            //get roles
            $textquery = "
				SELECT  role_id
				FROM account_role
				WHERE account_id = '$result->account_id'
			 	";
            $roles = $this->CI->db->query($textquery)->result();
            foreach($roles as $role){
                $account_role[] = $role->role_id;
            }
            $result->role = $account_role;

            //get privileges by role id
            $textquery = "
				SELECT  distinct b.privilege_id
				FROM account_role a
				INNER JOIN role_privilege b on a.role_id = b.role_id
				WHERE account_id = '$result->account_id'
				ORDER BY  b.privilege_id
			 	";

            if(in_array('admin', $account_role)){ // if admin she/he got all privileges
                $textquery = "
					SELECT  distinct privilege_id
					FROM privilege
				 	";
            }
            $privileges = $this->CI->db->query($textquery)->result();
            foreach($privileges as $privilege){
                $account_privilege[] = $privilege->privilege_id;
            }
            $result->privilege = $account_privilege;
            $result->groups = $this->groups_detail($result->account_id, $groups);

        }

        return $result;
    }


    public function change_password($account_id, $old_password, $new_password) {
        $old_password = md5('mat' . $old_password . 'kra');
        $new_password = md5('mat' . $new_password . 'kra');

        $textquery = "
			UPDATE account set password='$new_password'
			WHERE account_id = '$account_id'
			AND password = '$old_password'
		 	";
        $query = $this->db->query($textquery);
    }

    public function account_list($keyword){
        $textquery = "
			SELECT
				account_id, user_id, email, display_name, fname, lname, active, last_login, created
			FROM account a
			WHERE lower(user_id) like lower('%$keyword%')
						or lower(email) like lower('%$keyword%')
						or lower(display_name) like lower('%$keyword%')
			ORDER BY display_name
		 	";
        $rows = $this->db->query($textquery)->result();
        return $rows;
    }

    public function account_detail($account_id){
        $textquery = "
			SELECT
				account_id, user_id, email, display_name, fname, lname, active, account_type_id, last_login
			FROM account a
			WHERE account_id = '$account_id'
		 	";
        $rows = $this->db->query($textquery)->row();
        if ($rows) {
            $textquery = "
				SELECT  distinct  b.role_id
				FROM account_role a
				INNER JOIN role b on a.role_id = b.role_id
				WHERE account_id = '$account_id'
				ORDER BY b.role_id
			 	";
            $roles = $this->db->query($textquery)->result();
            $account_role = array();
            foreach($roles as $role){
                $account_role[] = strtolower($role->role_id);
            }
            $rows->role = $account_role;
        }
        return $rows;
    }


    public function account_save($data){
        $textquery = "
			SELECT user_id FROM account WHERE lower(email) = lower('$data[email]')
			";

        $rows = $this->db->query($textquery)->row();
        if($rows){
            $data['rcode'] = 0;
            $data['message'] = "Email Already Exist '$data[email]'";
        }else{
            $account_id = sha1( uniqid() );
            $password = md5('mat' . $data['password'] . 'kra');
            $textquery = "
				INSERT INTO account (account_id, user_id, password, email, fname, lname, display_name, active, account_type_id, created)
				VALUES ('$account_id', '$data[email]', '$password', '$data[email]', '$data[fname]', '$data[lname]', '$data[display_name]', '1', '$data[account_type_id]', now())
			 	";
            $query = $this->db->query($textquery);
            $data['rcode'] = 1;
            $data['message'] = "Successfully Created Account '$data[email]'";
        }
        return $data;
    }


    public function account_delete($data){
        $textquery = "
			DELETE FROM account WHERE account_id = lower('$data[account_id]')
			";

        $query = $this->db->query($textquery);
    }

    public function account_detail_save($data){

        if ($data['account_id'] =='0') {
            $account_id = sha1( uniqid() );
            $password = md5('mat123456kra');
            $textquery = "
				INSERT INTO account (account_id, user_id, password, email, fname, lname, display_name, active, account_type_id, created)
				VALUES ('$account_id', '$data[email]', '$password', '$data[email]', '$data[fname]', '$data[lname]', '$data[display_name]', '1', '$data[account_type_id]', now())
			 	";
            //$query = $this->db->query($textquery);
        } else {
            $account_id = $data['account_id'];

            $textquery = "
				UPDATE account set
						fname = '$data[fname]',
						lname = '$data[lname]',
						display_name = '$data[display_name]',
						email = '$data[email]',
						active = '$data[active]',
						account_type_id = '$data[account_type_id]'
				WHERE account_id = '$account_id'
			 	";
            $query = $this->db->query($textquery);
        }

        $textquery = "DELETE FROM account_role WHERE account_id = '$account_id'";
        $query = $this->db->query($textquery);

        if (!empty($data['role_id'])) {
            foreach($data['role_id'] as $role){
                $textquery = "INSERT INTO account_role (account_id, role_id) VALUES ('$account_id', '$role') ";
                $query = $this->db->query($textquery);
            }
        }
    }


    public function profile_save($data){

        $account_id = $data['account_id'];

        $textquery = "
				UPDATE account set
						fname = '$data[fname]',
						lname = '$data[lname]',
						display_name = '$data[display_name]',
						email = '$data[email]'
				WHERE account_id = '$account_id'
			 	";
        $query = $this->db->query($textquery);
    }

    public function account_type_list(){
        $textquery = "
			SELECT
				account_type_id, account_type_name
			FROM account_type
			ORDER BY order_number
		 	";
        $rows = $this->db->query($textquery)->result();

        return $rows;
    }


    public function role_list(){
        $textquery = "
			SELECT
				role_id, role_name
			FROM role
			ORDER BY role_name
		 	";
        $rows = $this->db->query($textquery)->result();

        foreach($rows as $row){
            $textquery = "
				SELECT
					a.privilege_id, privilege_name
				FROM role_privilege a
				INNER JOIN privilege b on a.privilege_id = b.privilege_id
				WHERE role_id = '$row->role_id'
				ORDER BY  b.privilege_name
			 	";
            $privilege_list = $this->db->query($textquery)->result();
            $privilege = '';
            $i=0;
            foreach($privilege_list as $list){
                if ($i > 0) $privilege .= ', ';
                $privilege .= str_replace('Manage ','', $list->privilege_name );
                $i++;
            }
            $row->role_name .= '  ('.$privilege .')';
        }

        return $rows;
    }


    function get_useraccountid($username) {
        $sql = "
			SELECT
				account_id, user_id, email, fname, lname,
				display_name, phone, birthdate, gender, thumbnail, active,
				last_login, account_type_id
			FROM account
			WHERE user_id LIKE '%$username%'
			AND active = 1
		 	";
        $result = $this->CI->db->query($sql)->row();

        if($result)
        {
            $account_id = $result->account_id;
            $user_id = $result->user_id;

            $sql = "SELECT g.groups_id, g.groups_name FROM groups g
                    INNER JOIN groups_account ga ON g.`groups_id` = ga.`groups_id`
                    WHERE ga.`account_id` = '$account_id'";
            $resGroup = $this->CI->db->query($sql)->result_array();

        }

        return $resGroup;
    }

    function groups_detail($account_id, $groups_id){
        $textquery = "
			SELECT
				a.groups_id, b.groups_name, b.working_dir, b.streaming_dir
			FROM groups_account a, groups b
			WHERE a.account_id = '$account_id'
			AND a.groups_id = '$groups_id'
			AND a.groups_id = b.groups_id
		 	";
        $row = $this->CI->db->query($textquery)->row();
        return $row;
    }

    function groups_channel($groups_id) {
        $sql = "SELECT gc.`groups_id`, c.channel_id, c.channel_name
                FROM channel c
                INNER JOIN groups_channel gc ON gc.channel_id = c.channel_id
                WHERE gc.groups_id = '$groups_id'";
        $row = $this->CI->db->query($sql)->result_array();
        return $row;
    }

    function groups_apps($groups_id) {
        $sql = "SELECT ga.`groups_id`, a.apps_id, a.app_name
                FROM apps a
                INNER JOIN groups_apps ga ON ga.apps_id = a.apps_id
                WHERE ga.groups_id = '".$groups_id."'";
        $row = $this->CI->db->query($sql)->result_array();
        return $row;
    }

}