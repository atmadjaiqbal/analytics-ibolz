<?php
class Utils extends Application{
	public function __construct()
	{
		parent::__construct();
	}

	public function bootbox()
	{
		$data = [];

		$html['html']['content']  = $this->load->view('utils/bootbox', $data, TRUE);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, TRUE);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, TRUE);
        $this->load->view('template/tpl_one_column', $html, false);

	}

	public function upload1()
	{
		$data = [];
		$html['html']['content']  = $this->load->view('utils/upload1', $data, TRUE);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, TRUE);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, TRUE);
        $this->load->view('template/tpl_one_column', $html, false);
	}
}


/*
EOF
*/