<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Analytic extends Application {

    public function __construct() {
        parent::__construct();
        $data = $this->data;
    }

    public function index() {
        $channel_id = !empty($_GET['channel_id']) ? $_GET['channel_id'] : '1767690419551a4c9543afa';
        $start = !empty($_GET['start']) ? $_GET['start'] : date('Y-m-d');
        $end = !empty($_GET['end']) ? $_GET['end'] : date('Y-m-d');
        $data['chart_type'] = !empty($_GET['chart_type']) ? $_GET['chart_type'] : 'column'; 
        $data['start'] = $start;
        $data['end'] = $end;
        
        // $data['scripts'] = array('highcharts.js');
        $res = $this->load->model('ibolz_model');
        $data['channels'] = $this->ibolz_model->get_channels();
        $data['channel_id'] = $channel_id;

        $graph_analytics = $this->ibolz_model->graph_analytics($channel_id,$start,$end);

        $data['days'] = '[]';
        $data['mobile'] = '[]';
        $data['client'] = '[]';
        $data['duration'] = '[]';

        $days = '[]'; 
        $mobile = '[]';
        $duration = '[]';
        if(!empty($graph_analytics)){
            $days = '';
            $mobile = '';
            $client = '';
            $duration = '';

            foreach($graph_analytics as $var){
                $days .= '"'.$var->showndate.'"'.',';
                $mobile .= $var->nb_mobile_count.',';
                $client .= $var->nb_client_count.',';
                $duration .= $var->nb_duration.',';
                  
            };
            $data['days'] = rtrim($days,',');
            $data['mobile'] = rtrim($mobile,',');
            $data['client'] = rtrim($client,',');
            $data['duration'] = rtrim($duration,',');
        }        


        // $html['html']['content']  = $this->load->view('analytic/analytic_dashboard', $data, true);
        $html['html']['content']  = $this->load->view('analytic/analytic_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }

    // public function update_page(){
    //     $channel_id = !empty($_POST['channel_id']) ? $_POST['channel_id'] : 0;

    //     header('Content-Type: application/json');
    //     echo json_encode($channel_id);        
    // }




}