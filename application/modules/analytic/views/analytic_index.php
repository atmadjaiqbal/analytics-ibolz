<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css"/>
<script src="<?php echo $this->config->base_url('assets/js/highcharts.js'); ?>"></script>
<script src="<?php echo $this->config->base_url('assets/js/exporting.js'); ?>"></script>

<script type="text/javascript" src="<?php echo $this->config->base_url('assets/js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->config->base_url('assets/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css"/>

<script src="<?php echo $this->config->base_url('assets/js/custom.js'); ?>"></script>
<form method="get" id="frm_main">
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <h4>Chart Type</h4>
        </div>

        <div class="col-md-10 text-left">
            <div >
                <select class="form-control" id="chart_selector" name="chart_type">
                    <option value="column" <?php echo $chart_type=='column' ? 'selected' : '';?>>Column</option>
                    <option value="line" <?php echo $chart_type=='line' ? 'selected' : '';?>>Line</option>
                    <option value="spline" <?php echo $chart_type=='spline' ? 'selected' : '';?>>Spline</option>
                    <option value="area" <?php echo $chart_type=='area' ? 'selected' : '';?>>Area</option>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <h4>Channel</h4>
        </div>

        <div class="col-md-10 text-left">
            <div >
                <select class="form-control" id="channel_selector" name="channel_id">
                <?php 
                if(!empty($channels)){
                    foreach($channels as $v){
                        $sel = $channel_id == $v->channel_id ? 'selected' : '';
                        echo '<option value="'.$v->channel_id.'" '.$sel.'><a href="#">'.$v->channel_name.'</a></option>';
                    }
                }
                ?>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h4>Start Date - End Date</h4>
        </div>
    </div>
    <div class="row">
        <div class='col-md-6'>
            <div class="form-group">
                <div class='input-group date' >
                    <input type='text' class="form-control" placeholder="Start Date" name="start" id='datetimepicker6' value="<?php echo $start;?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class='col-md-6'>
            <div class="form-group">
                <div class='input-group date' >
                    <input type='text' class="form-control" placeholder="End Date" name="end" id='datetimepicker7' value="<?php echo $end;?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <button class="btn btn-primary btn-block" type="submit" onclick="javascript:frm_main.submit();">Update</button>
        </div>
    </div>

</div>
</form>

<div class="container"> 
    <div class="col-md-12">
        <div id="chart_container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
</div>

<script type="text/javascript">
// $('#channel_selector').change(function(){
// 	var data = $(this).val();
//     $.ajax({
//         ajax: 'true',
//         async: 'true',
//         type: 'post',
//         dataType: "json",
//         data: {'channel_id':data},
//         url: '/analytic/update_page',
//         beforeSend: function(){},
//         success: function(response){
//         	alert(response);
//         },
//         complete: function(){}
//     })
// })

$(function () {
    $('#datetimepicker6').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#datetimepicker7').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent: false //Important! See issue #1075
    });
    $("#datetimepicker6").on("dp.change", function (e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker7").on("dp.change", function (e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });

    $('#chart_container').highcharts({
        chart: {
            type: '<?php echo $chart_type;?>'
        },
        title: {
            text: 'Ibolz Statistics'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [<?php echo $days;?>],
            title: "wwww",
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Hit'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
        {
            name: 'Mobile',
            data: [<?php echo $mobile;?>]

        }, 
        {
            name: 'Client',
            data: [<?php echo $client;?>]

        }, 
        {
            name: 'Duration',
            data: [<?php echo $duration;?>]

        }
        ]
    });


   
});

</script>