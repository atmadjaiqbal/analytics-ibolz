<div class="main" id="main-page">
    <div class="page-header">
        <h3><?php echo $heading;?></h3>
        <div class="page-content">

           <div class="col-md-4">
              <form class="form-horizontal" method="get" id="frm_main" action="<?php echo base_url();?>home/statistik_channel">
                  <div class="form-group">
                      <label class="col-sm-5 control-label" for="chart_selector">Chart Type</label>
                      <div class="col-sm-7">
                          <select class="form-control" id="chart_selector" name="chart_type">
                              <option value="column" <?php echo $chart_type=='column' ? 'selected' : '';?>>Column</option>
                              <option value="line" <?php echo $chart_type=='line' ? 'selected' : '';?>>Line</option>
                              <option value="spline" <?php echo $chart_type=='spline' ? 'selected' : '';?>>Spline</option>
                              <option value="area" <?php echo $chart_type=='area' ? 'selected' : '';?>>Area</option>
                          </select>
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-sm-5 control-label" for="channel_selector">Channel</label>
                      <div class="col-sm-7">
                          <select class="form-control" id="channel_selector" name="channel_id">
                              <?php
                              if($row_channel)
                              {
                                  foreach($row_channel as $v){
                                      $sel = $channel_id == $v['channel_id'] ? 'selected' : '';
                                      echo '<option value="'.$v['channel_id'].'" '.$sel.'><a href="#">'.$v['channel_name'].'</a></option>';
                                  }
                              }
                              ?>
                          </select>
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-sm-5 control-label" for="channel_selector">Start Date - End Date</label>
                      <div class="col-sm-7">
                          <div class="input-daterange input-group">
                              <input type="text" class="input-sm form-control" id='dtpickStart' name="start" value="<?php echo $start;?>"/>
                              <span class="input-group-addon">to</span>
                              <input type="text" class="input-sm form-control" id='dtpickEnd' name="end" value="<?php echo $end;?>"/>
                          </div>
                      </div>
                  </div><br/>

                  <div class="form-group">
                      <div class="col-sm-3 text-right">
                         <button class="btn btn-primary btn-block" type="submit" onclick="javascript:frm_main.submit();">Update</button>
                      </div>
                  </div>
              </form>

           </div>

           <div class="col-md-8">


           </div>

           <div class="col-md-12">
               <div id="chart_container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
           </div>

           <br/>

           <div class="col-md-12" style="margin-top:20px;border-top: 1px;">
               <div class="col-md-6">
                   <div>
                       <button class="btn btn-primary" type="button">
                           Total Mobile Hit <span class="badge"><?php echo !empty($nb_total_mobiles) ? $nb_total_mobiles : 0;?> hits</span>
                       </button>
                   </div>

                   <div>
                       <button class="btn btn-primary" type="button">
                           Total Client Hit <span class="badge"><?php echo !empty($nb_total_clients) ? $nb_total_clients : 0;?> hits</span>
                       </button>
                   </div>

                   <div>
                       <button class="btn btn-primary" type="button">
                           Total Duration <span class="badge"><?php echo !empty($nb_total_durations) ? $nb_total_durations : 0;?> minutes</span>
                       </button>
                   </div>

               </div>

               <div class="col-md-6">
                   <div id="pie_container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
               </div>

           </div>



            <script type="text/javascript">

                $(function () {
                    $('#dtpickStart').datetimepicker({
                        format: 'YYYY-MM-DD'
                    });
                    $('#dtpickEnd').datetimepicker({
                        format: 'YYYY-MM-DD',
                        useCurrent: false //Important! See issue #1075
                    });

                    $("#dtpickStart").on("dp.change", function (e) {
                        $('#dtpickEnd').data("DateTimePicker").minDate(e.date);
                    });

                    $("#dtpickEnd").on("dp.change", function (e) {
                        $('#dtpickStart').data("DateTimePicker").maxDate(e.date);
                    });

                    $('#chart_container').highcharts({
                        credits: {
                            enabled: false
                        },
                        chart: {
                            type: '<?php echo $chart_type;?>'
                        },
                        title: {
                            text: 'Ibolz Statistics'
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: [<?php echo $days;?>],
                            title: "wwww",
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Hit'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [
                            {
                                name: 'Mobile',
                                data: [<?php echo $mobile;?>]

                            },
                            {
                                name: 'Client',
                                data: [<?php echo $client;?>]

                            },
                            {
                                name: 'Duration',
                                data: [<?php echo $duration;?>]

                            }
                        ]
                    });

                    $('#pie_container').highcharts({
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b>'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: "",
                            colorByPoint: true,
                            data: [{
                                name: "Mobile",
                                y: <?php echo !empty($nb_total_mobiles) ? $nb_total_mobiles : 0;?>
                            }, {
                                name: "Client",
                                y: <?php echo !empty($nb_total_clients) ? $nb_total_clients : 0;?>,
                                //sliced: true,
                                //selected: true
                            }, {
                                name: "Durations",
                                y: <?php echo !empty($nb_total_durations) ? $nb_total_durations : 0;?>
                            }
                            ]
                        }]
                    });

                });

            </script>
        </div>
    </div>
</div>



