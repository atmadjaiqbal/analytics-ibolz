<div class="main" id="main-page">
    <div class="page-header">
        <h3><?php echo $heading;?></h3>
        <div class="page-content">

           <div class="col-md-4">
              <form class="form-horizontal" method="get" id="frm_main" action="<?php echo base_url();?>home/statistik_content">
                  <div class="form-group">
                      <label class="col-sm-5 control-label" for="chart_selector">Chart Type</label>
                      <div class="col-sm-7">
                          <select class="form-control" id="chart_selector" name="chart_type">
                              <option value="column" <?php echo $chart_type=='column' ? 'selected' : '';?>>Column</option>
                              <option value="line" <?php echo $chart_type=='line' ? 'selected' : '';?>>Line</option>
                              <option value="spline" <?php echo $chart_type=='spline' ? 'selected' : '';?>>Spline</option>
                              <option value="area" <?php echo $chart_type=='area' ? 'selected' : '';?>>Area</option>
                          </select>
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-sm-5 control-label" for="channel_selector">App</label>
                      <div class="col-sm-7">
                          <select class="form-control" id="app_selector" name="selected_apps_id">
                            <option value="all">All</option>
                          <?php 
                          if(!empty($row_apps)){
                            foreach($row_apps as $v){
                              $sel = $v['apps_id'] == $selected_apps_id ? 'selected' : '';
                              echo '<option value="'.$v['apps_id'].'" '.$sel.'>'.$v['app_name'].'</option>';
                            }
                          }
                          ?>
                          </select>
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-sm-3 text-right">
                         <button class="btn btn-primary btn-block" type="submit" onclick="javascript:frm_main.submit();">Update</button>
                      </div>
                  </div>
              </form>

           </div>

           <div class="col-md-8">


           </div>

           <div class="col-md-12">
               <div id="chart_container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
           </div>

           <br/>

           <div class="col-md-12" style="margin-top:20px;border-top: 0px;">
               <div class="col-md-6">
                   <div>
                       <button class="btn btn-primary" type="button">
                           Total Device Installed <span class="badge"><?php echo !empty($nb_device_installed) ? $nb_device_installed : 0;?> </span>
                       </button>
                   </div>

                   <div>
                       <button class="btn btn-primary" type="button">
                           Total Device Online <span class="badge"><?php echo !empty($nb_device_online) ? $nb_device_online : 0;?> </span>
                       </button>
                   </div>

                   <div>
                       <button class="btn btn-primary" type="button">
                           Total Customer <span class="badge"><?php echo !empty($nb_customer) ? $nb_customer : 0;?> </span>
                       </button>
                   </div>

                   <div>
                       <button class="btn btn-primary" type="button">
                           Total Online Customer <span class="badge"><?php echo !empty($nb_online_customer) ? $nb_online_customer : 0;?> </span>
                       </button>
                   </div>


               </div>

               <div class="col-md-6">
                   <div id="pie_container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
               </div>

           </div>

<script type="text/javascript">

$(function () {
    $('#chart_container').highcharts({
        chart: {
            type: '<?php echo $chart_type;?>'  
        },
        title: {
            text: 'Ibolz Statistics'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [<?php echo $xaxis;?>],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Hit'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
        {
            name: 'Device',
            data: [<?php echo !empty($x_device_installed) ? $x_device_installed : 0;?>]

        }, 
        {
            name: 'Device Online',
            data: [<?php echo !empty($x_device_online) ? $x_device_online : 0;?>]

        }, 
        {
            name: 'Customer',
            data: [<?php echo !empty($x_customer) ? $x_customer : 0;?>]

        },
        {
            name: 'Customer Online',
            data: [<?php echo !empty($x_online_customer) ? $x_online_customer : 0;?>]

        }

        ]
    });

     $('#pie_container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: "",
            colorByPoint: true,
            data: [
                {
                name: "Device Installed",
                y: <?php echo !empty($nb_device_installed) ? $nb_device_installed : 0;?>                            
                }, 
                {
                name: "Device Online",
                y: <?php echo !empty($nb_device_online) ? $nb_device_online : 0;?>,
                //sliced: true,
                //selected: true
                }, 
                {
                name: "Customer",
                y: <?php echo !empty($nb_customer) ? $nb_customer : 0;?>                            
                },
                {
                name: "Customer Online",
                y: <?php echo !empty($nb_online_customer) ? $nb_online_customer : 0;?>                            
                }

            ]
        }]
    });


   
});

</script>

        </div>
    </div>
</div>



