<div class="main" id="main-page">
    <div class="page-header">
        <h3><?php echo $heading;?></h3>
        <div class="page-content">
          <div class="row">
            <div class="col-md-4">
                <form class="form-horizontal" method="get" id="frm_main" action="<?php echo base_url();?>home/statistik_content_by_app">
                  <div class="form-group">
                      <label class="col-sm-5 control-label" for="chart_selector">Chart Type</label>
                      <div class="col-sm-7">
                          <select class="form-control" id="chart_selector" name="chart_type">
                              <option value="column" <?php echo $chart_type=='column' ? 'selected' : '';?>>Column</option>
                              <option value="line" <?php echo $chart_type=='line' ? 'selected' : '';?>>Line</option>
                              <option value="spline" <?php echo $chart_type=='spline' ? 'selected' : '';?>>Spline</option>
                              <option value="area" <?php echo $chart_type=='area' ? 'selected' : '';?>>Area</option>
                          </select>
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-sm-5 control-label" for="channel_selector">App</label>
                      <div class="col-sm-7">
                          <select class="form-control" id="app_selector" name="s_apps_id">
                          <?php 
                          if(!empty($row_apps)){
                            foreach($row_apps as $v){
                              $sel = $v['apps_id'] == $s_apps_id ? 'selected' : '';
                              echo '<option value="'.$v['apps_id'].'" '.$sel.'>'.$v['app_name'].'</option>';
                            }
                          }
                          ?>
                          </select>
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-sm-5 control-label" for="channel_selector">Contents</label>
                      <div class="col-sm-7">
                          <div id="content_selector">
                          <select class="form-control" id="content_selector" name="s_content_id">
                          <?php 
                          if(!empty($row_contents)){
                            foreach($row_contents as $rc){
                                $s = $rc['content_id'] == $s_content_id ? 'selected' : ''; 
                                echo '<option value="'.$rc['content_id'].'" '.$s.'>'.$rc['title'].'</option>';
                            }
                          }else{?>
                          <option>Select Apps First</option>
                          <?php  }?>
                          
                          </select>
                          </div>
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-sm-5 control-label" for="channel_selector">Start Date - End Date</label>
                      <div class="col-sm-7">
                          <div class="input-daterange input-group">
                              <input type="text" class="input-sm form-control" id='dtpickStart' name="start" value="<?php echo $start;?>"/>
                              <span class="input-group-addon">to</span>
                              <input type="text" class="input-sm form-control" id='dtpickEnd' name="end" value="<?php echo $end;?>"/>
                          </div>
                      </div>
                  </div><br/>

                  <div class="form-group">
                      <div class="col-sm-3 text-right">
                         <button class="btn btn-primary btn-block" type="submit" onclick="javascript:frm_main.submit();">Update</button>
                      </div>
                  </div>
              </form>
            </div>
            <div class="col-md-8">
              &nbsp;
            </div>            
          </div>

          <div class="row">
            <div class="col-md-12">
               <div id="chart_container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
               <div id="chart_container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
            <div class="col-md-6">
               <div id="chart_container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6"><br/><br/></div>
          </div>
          <div class="row">
            <div class="col-md-6">
                <div>
                     <button class="btn btn-primary" type="button">
                         Total Likes <span class="badge"><?php echo !empty($nb_total_likes) ? $nb_total_likes : 0;?></span>
                     </button>
                 </div>

                 <div>
                     <button class="btn btn-primary" type="button">
                         Total Viewers <span class="badge"><?php echo !empty($nb_total_viewers) ? $nb_total_viewers : 0;?></span>
                     </button>
                 </div>

                 <div>
                     <button class="btn btn-primary" type="button">
                         Total Comments <span class="badge"><?php echo !empty($nb_total_comment) ? $nb_total_comment : 0;?></span>
                     </button>
                 </div>            
            </div>
            <div class="col-md-6">
              <div id="pie_container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
            </div>
          </div>
        </div>


        



    </div>
</div>
<script type="text/javascript">
$('#app_selector').change(function(){
  var data = {'selected_apps_id':$(this).val()};
  $.ajax({
      ajax: 'true',
      async: 'true',
      type: 'post',
      dataType: 'json',
      data: data,
      url: '/home/ajax_content_list',
      beforeSend: function(){},
      success: function(response){
        if(response.result == 1){
          $('#content_selector').html(response.msg);  
        }else{
          alert(response.msg);
        }
      },
      complete: function(){}
    })

})


$(function () {
    $('#dtpickStart').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#dtpickEnd').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent: false //Important! See issue #1075
    });

    $("#dtpickStart").on("dp.change", function (e) {
        $('#dtpickEnd').data("DateTimePicker").minDate(e.date);
    });

    $("#dtpickEnd").on("dp.change", function (e) {
        $('#dtpickStart').data("DateTimePicker").maxDate(e.date);
    });

    $('#chart_container1').highcharts({
        credits: {
            enabled: false
        },
        chart: {
            type: '<?php echo $chart_type;?>'
        },
        title: {
            text: 'Likes'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [<?php echo $xaxis1;?>],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'NB'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            {
                name: '<?php echo $legend1;?>',
                data: [<?php echo $serie_value1;?>]

            }
        ]
    });

    $('#chart_container2').highcharts({
        credits: {
            enabled: false
        },
        chart: {
            type: '<?php echo $chart_type;?>'
        },
        title: {
            text: 'Comments'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [<?php echo $xaxis2;?>],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'NB'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            {
                name: '<?php echo $legend2;?>',
                data: [<?php echo $serie_value2;?>]

            }
        ]
    });

    $('#chart_container3').highcharts({
        credits: {
            enabled: false
        },
        chart: {
            type: '<?php echo $chart_type;?>'
        },
        title: {
            text: 'Viewers'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [<?php echo $xaxis3;?>],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'NB'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            {
                name: '<?php echo $legend3;?>',
                data: [<?php echo $serie_value3;?>]

            }
        ]
    });


    $('#pie_container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: "",
            colorByPoint: true,
            data: [{
                name: "Likes",
                y: <?php echo $nb_total_likes;?>
              }, {
                name: "Viewers",
                y: <?php echo $nb_total_viewers;?>,
                //sliced: true,
                //selected: true
            },{
                name: "Comment",
                y: <?php echo $nb_total_comment;?>,
                //sliced: true,
                //selected: true
            },  
            ]
        }]
    });

});

</script>


