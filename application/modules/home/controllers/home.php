<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Application {

    public function __construct() {
        parent::__construct();
        $data = $this->data;
        $this->load->library('login/account_lib');
        $this->load->library('home_lib');

    }

    public function index()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['heading'] = "Statistik Channel";
            $data['scripts'] = array('highcharts.js');

            $role = $this->session->userdata['logged_in']['account']->role[0];
            $groups_id = isset($this->session->userdata['logged_in']['account']->groups->groups_id) ? $this->session->userdata['logged_in']['account']->groups->groups_id : '';
            $data['rowapps'] = $this->home_lib->get_apps_by_groups($role, $groups_id);

            $html['html']['content']  = $this->load->view('home/home_index', $data, TRUE);
            $html['html']['header']   = $this->load->view('template/tpl_header', $html, TRUE);
            $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, TRUE);
            $this->load->view('template/tpl_one_column', $html, false);
        }  else {
            redirect('login', 'refresh');
        }
    }

    public function statistik_channel() {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['heading'] = "Statistik Channel";
            $data['scripts'] = array('highcharts.js');

            $groups_id = $this->session->userdata['logged_in']['account']->groups->groups_id;
            $data['row_channel'] = $this->account_lib->groups_channel($groups_id);

            if(isset($data['row_channel'][0]['channel_id'])) { $chanid = $data['row_channel'][0]['channel_id']; } else { $chanid = 0; }
            $channel_id = !empty($_GET['channel_id']) ? $_GET['channel_id'] : $chanid;
            $data['chart_type'] = !empty($_GET['chart_type']) ? $_GET['chart_type'] : 'column';
            $start = !empty($_GET['start']) ? $_GET['start'] : date('Y-m-d');
            $end = !empty($_GET['end']) ? $_GET['end'] : date('Y-m-d');
            $data['start'] = $start;
            $data['end'] = $end;

            $data['nb_total_days'] = 0;
            $data['nb_total_mobiles'] = 0;
            $data['nb_total_clients'] = 0;
            $data['nb_total_durations'] = 0;


            $res = $this->load->model('ibolz_model');
            $data['channels'] = $this->ibolz_model->get_channels();
            $data['channel_id'] = $channel_id;

            $graph_analytics = $this->ibolz_model->graph_analytics($channel_id,$start,$end);

            $data['days'] = '[]';
            $data['mobile'] = '[]';
            $data['client'] = '[]';
            $data['duration'] = '[]';

            $days = '[]';
            $mobile = '[]';
            $duration = '[]';
            if(!empty($graph_analytics)){
                $days = '';
                $mobile = '';
                $client = '';
                $duration = '';

                foreach($graph_analytics as $var){
                    $data['nb_total_days'] = $data['nb_total_days'] + 1;
                    $data['nb_total_mobiles'] = $data['nb_total_mobiles'] + $var->nb_mobile_count;
                    $data['nb_total_clients'] = $data['nb_total_clients'] + $var->nb_client_count;
                    $data['nb_total_durations'] = $data['nb_total_durations'] + $var->nb_duration;

                    $days .= '"'.$var->showndate.'"'.',';
                    $mobile .= $var->nb_mobile_count.',';
                    $client .= $var->nb_client_count.',';
                    $duration .= $var->nb_duration.',';

                };
                $data['days'] = rtrim($days,',');
                $data['mobile'] = rtrim($mobile,',');
                $data['client'] = rtrim($client,',');
                $data['duration'] = rtrim($duration,',');
            }

            $html['html']['content']  = $this->load->view('home/home_dashboard', $data, TRUE);
            $html['html']['header']   = $this->load->view('template/tpl_header', $html, TRUE);
            $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, TRUE);
            $this->load->view('template/tpl_one_column', $html, false);
        }  else {
            redirect('login', 'refresh');
        }
    }

    public function channel_statistik() {
        $channel_id = !empty($_GET['channel_id']) ? $_GET['channel_id'] : '1767690419551a4c9543afa';
        $data['scripts'] = array('highcharts.js');
        $res = $this->load->model('ibolz_model');
        $data['channels'] = $this->ibolz_model->get_channels();
        $data['channel_id'] = $channel_id;

        $data['graph_mobile'] = $this->ibolz_model->graph_mobile($channel_id);
        $data['graph_client'] = $this->ibolz_model->graph_client($channel_id);
        $data['graph_duration'] = $this->ibolz_model->graph_duration($channel_id);

        // $html['html']['content']  = $this->load->view('analytic/analytic_dashboard', $data, true);
        $html['html']['content']  = $this->load->view('analytic/analytic_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function statistik_content() {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['heading'] = "Statistik Content";
            $data['scripts'] = array('highcharts.js');

            $data['nb_device_installed'] = 0;
            $data['nb_device_online'] = 0;
            $data['nb_customer'] = 0;
            $data['nb_online_customer'] = 0;

            $groups_id = $this->session->userdata['logged_in']['account']->groups->groups_id;

            if($this->session->userdata['logged_in']['account']->role[0] == 'admin'){
                // $data['row_apps'] = $this->home_lib->get_apps();
                $data['row_apps'] = $this->account_lib->groups_apps($groups_id);
            }else{
                $data['row_apps'] = $this->account_lib->groups_apps($groups_id);
            }

            $data['selected_apps_id'] = !empty($_GET['selected_apps_id']) ? $_GET['selected_apps_id'] : $data['row_apps'][0]['apps_id'];
            if($data['selected_apps_id'] == 'all'){
                $data['apps_id'] = $data['row_apps'][0]['apps_id'];
            }else{
                $data['apps_id'] = $data['selected_apps_id'];
            }

            // $statistics = $this->home_lib->count_device_by_apps_installed(['"com.balepoint.ibolz.airport"','"com.balepoint.ibolz.alazhar"','"com.balepoint.ibolz.allianz"','"com.balepoint.ibolz.ap"']);
            $data['chart_type'] = !empty($_GET['chart_type']) ? $_GET['chart_type'] : 'column';

            if($data['selected_apps_id'] != 'all'){
                $statistics = $this->home_lib->count_device_by_apps_installed(['"'.$data['apps_id'].'"']);
            }else{
                $request = [];
                $xnames = [];
                if(!empty($data['row_apps'])){
                    foreach($data['row_apps'] as $arr){
                        $request[] = '"'.$arr['apps_id'].'"';
                        $xnames[] = '"'.$arr['app_name'].'"';
                    };
                };    
                $statistics = $this->home_lib->count_device_by_apps_installed($request);
            }

            $data['x_device_installed'] = '';
            $data['x_device_online'] = '';
            $data['x_customer'] = '';
            $data['x_online_customer'] = '';

            if(!empty($statistics)){
                foreach($statistics as $s){
                    $data['nb_device_installed'] = $data['nb_device_installed'] + $s['total_device'];
                    $data['nb_device_online'] = $data['nb_device_online'] + $s['total_device_online'];
                    $data['nb_customer'] = $data['nb_customer'] + $s['total_customer'];
                    $data['nb_online_customer'] = $data['nb_online_customer'] + $s['total_customer_online'];

                    $data['x_device_installed'] .= $s['total_device'].',';
                    $data['x_device_online'] .= $s['total_device_online'].',';
                    $data['x_customer'] .= $s['total_customer'].',';
                    $data['x_online_customer'] .= $s['total_customer_online'].',';

                }

                $data['x_device_installed'] = rtrim($data['x_device_installed'],',');
                $data['x_device_online'] = rtrim($data['x_device_online'],',');
                $data['x_customer'] = rtrim($data['x_customer'],',');
                $data['x_online_customer'] = rtrim($data['x_online_customer'],',');
                   
            }

            if($data['selected_apps_id'] != 'all'){
                $data['xaxis'] = '"'.$data['apps_id'].'"';
            }else{
                 $data['xaxis'] = implode(',', $xnames);
            }
           

            $html['html']['content']  = $this->load->view('home/content_statistic', $data, TRUE);
            $html['html']['header']   = $this->load->view('template/tpl_header', $html, TRUE);
            $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, TRUE);
            $this->load->view('template/tpl_one_column', $html, false);
        }  else {
            redirect('login', 'refresh');
        }
    }

    public function statistik_content_by_app() {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['heading'] = "Statistik Content by Apps";
            $data['scripts'] = array('highcharts.js');

            $data['chart_type'] = !empty($_GET['chart_type']) ? $_GET['chart_type'] : 'column';
            $data['start'] = !empty($_GET['start']) ? $_GET['start'] : date('Y-m-d');
            $data['end'] = !empty($_GET['end']) ? $_GET['end'] : date('Y-m-d');

            $groups_id = $this->session->userdata['logged_in']['account']->groups->groups_id;
            if($this->session->userdata['logged_in']['account']->role[0] == 'admin'){
                // $data['row_apps'] = $this->home_lib->get_apps();
                $data['row_apps'] = $this->account_lib->groups_apps($groups_id);
            }else{
                $data['row_apps'] = $this->account_lib->groups_apps($groups_id);
            }

            $data['s_apps_id'] = !empty($_GET['s_apps_id']) ? $_GET['s_apps_id'] : $data['row_apps'][0]['apps_id'];
            $data['s_content_id'] = !empty($_GET['s_content_id']) ? $_GET['s_content_id'] : '';
            $data['row_contents'] = [];

            if($data['s_apps_id']){
                $data['row_contents'] = $this->home_lib->get_content_list($data['s_apps_id']);
            } 


            $data['xaxis1'] = '';
            $data['legend1'] = ''; 
            $data['serie_value1'] = '';
            $data['nb_total_likes'] = 0;

            $data['xaxis2'] = '';
            $data['legend2'] = ''; 
            $data['serie_value2'] = '';
            $data['nb_total_comment'] = 0;

            $data['xaxis3'] = '';
            $data['legend3'] = ''; 
            $data['serie_value3'] = '';
            $data['nb_total_viewers'] = 0;

            if($data['s_content_id'] != ''){
                $like_statistic = $this->home_lib->get_like_range_by_content_id($data['s_content_id'],$data['start'],$data['end']);
                $comment_statistic = $this->home_lib->get_comments_by_content_id($data['s_content_id'],$data['start'],$data['end']);
                $viewers_statistic = $this->home_lib->get_viewers_by_content_id('AND ca.content_id="'.$data['s_content_id'].'"',$data['s_apps_id'],$data['start'],$data['end']);
                if($like_statistic){
                    foreach($like_statistic as $ls){
                        $data['xaxis1'] .= '"'.$ls['specific_date'].'",';
                        $data['serie_value1'] .= $ls['nb_like'].',';
                        $data['nb_total_likes'] =  $data['nb_total_likes'] + $ls['nb_like'];
                    }
                    $data['xaxis1'] = rtrim($data['xaxis1'],',');
                    $data['legend1'] = $like_statistic[0]['title']; 
                }else{
                    $data['legend1'] = 'No data';
                }

                if($comment_statistic){
                    foreach($comment_statistic as $cs){
                        $data['xaxis2'] .= '"'.$cs['created'].'",';
                        $data['serie_value2'] .= $cs['nb_comment'].',';
                        $data['nb_total_comment'] =  $data['nb_total_comment'] + $cs['nb_comment'];
                    }
                    $data['xaxis2'] = rtrim($data['xaxis2'],',');
                    $data['legend2'] = $comment_statistic[0]['title']; 
                }else{
                    $data['legend2'] = 'No data';
                }

                if($viewers_statistic){
                    foreach($viewers_statistic as $vs){
                        $data['xaxis3'] .= '"'.$vs['created'].'",';
                        $data['serie_value3'] .= $vs['nb'].',';
                        $data['nb_total_viewers'] =  $data['nb_total_viewers'] + $vs['nb'];
                    }

                    $data['xaxis3'] = rtrim($data['xaxis3'],',');
                    $data['legend3'] = $viewers_statistic[0]['title'];
                }else{
                    $data['legend3'] = 'No data';
                }


            }
            




            $html['html']['content']  = $this->load->view('home/content_statistic_by_app', $data, TRUE);
            $html['html']['header']   = $this->load->view('template/tpl_header', $html, TRUE);
            $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, TRUE);
            $this->load->view('template/tpl_one_column', $html, false);
        }  else {
            redirect('login', 'refresh');
        }
    }

    public function statistik_channel_by_app() {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['heading'] = "Statistik Channel by Apps";
            $data['scripts'] = array('highcharts.js');

            $data['chart_type'] = !empty($_GET['chart_type']) ? $_GET['chart_type'] : 'column';
            $data['start'] = !empty($_GET['start']) ? $_GET['start'] : date('Y-m-d');
            $data['end'] = !empty($_GET['end']) ? $_GET['end'] : date('Y-m-d');

            $groups_id = $this->session->userdata['logged_in']['account']->groups->groups_id;
            if($this->session->userdata['logged_in']['account']->role[0] == 'admin'){
                // $data['row_apps'] = $this->home_lib->get_apps();
                $data['row_apps'] = $this->account_lib->groups_apps($groups_id);
            }else{
                $data['row_apps'] = $this->account_lib->groups_apps($groups_id);
            }

            $data['s_apps_id'] = !empty($_GET['s_apps_id']) ? $_GET['s_apps_id'] : $data['row_apps'][0]['apps_id'];
            $data['s_channel_id'] = !empty($_GET['s_channel_id']) ? $_GET['s_channel_id'] : '';
            $data['row_channels'] = [];

            if($data['s_apps_id']){
                $data['row_channels'] = $this->home_lib->get_channel_list($data['s_apps_id']);
            } 


            $data['xaxis1'] = '';
            $data['legend1'] = ''; 
            $data['serie_value1'] = '';
            $data['nb_total_likes'] = 0;

            $data['xaxis2'] = '';
            $data['legend2'] = ''; 
            $data['serie_value2'] = '';
            $data['nb_total_comment'] = 0;

            $data['xaxis3'] = '';
            $data['legend3'] = ''; 
            $data['serie_value3'] = '';
            $data['nb_total_viewers'] = 0;

            if($data['s_channel_id'] != ''){
                $like_statistic = $this->home_lib->get_like_range_by_content_id($data['s_channel_id'],$data['start'],$data['end']);
                if($like_statistic){
                    foreach($like_statistic as $ls){
                        $data['xaxis1'] .= '"'.$ls['specific_date'].'",';
                        $data['serie_value1'] .= $ls['nb_like'].',';
                        $data['nb_total_likes'] =  $data['nb_total_likes'] + $ls['nb_like'];
                    }
                    $data['xaxis1'] = rtrim($data['xaxis1'],',');
                    $data['legend1'] = $like_statistic[0]['title']; 
                }else{
                    $data['legend1'] = 'No data';
                }
                // $data['xaxis1'] = '"2015-09-24","2015-07-30","2015-07-13","2015-06-24","2015-06-16"';
                // $data['serie_value1'] = '3,4,1,6,5';
                // $data['nb_total_likes'] =  13;

                $comment_statistic = $this->home_lib->get_comments_by_content_id($data['s_channel_id'],$data['start'],$data['end']);
                if($comment_statistic){
                    foreach($comment_statistic as $cs){
                        $data['xaxis2'] .= '"'.$cs['created'].'",';
                        $data['serie_value2'] .= $cs['nb_comment'].',';
                        $data['nb_total_comment'] =  $data['nb_total_comment'] + $cs['nb_comment'];
                    }
                    $data['xaxis2'] = rtrim($data['xaxis2'],',');
                    $data['legend2'] = $comment_statistic[0]['title']; 
                }else{
                    $data['legend2'] = 'No data';
                }
                // $data['xaxis2'] = '"2015-09-24","2015-07-30","2015-07-13","2015-06-24","2015-06-16"';
                // $data['serie_value2'] = '3,4,1,6,5';
                // $data['nb_total_comment'] =  13;


                $viewers_statistic = $this->home_lib->get_viewers_by_content_id2('AND ca.channel_id="'.$data['s_channel_id'].'"',$data['s_apps_id'],$data['start'],$data['end']);

                if($viewers_statistic){
                    foreach($viewers_statistic as $vs){
                        $data['xaxis3'] .= '"'.$vs['created'].'",';
                        $data['serie_value3'] .= $vs['nb'].',';
                        $data['nb_total_viewers'] =  $data['nb_total_viewers'] + $vs['nb'];
                    }

                    $data['xaxis3'] = rtrim($data['xaxis3'],',');
                    $data['legend3'] = $viewers_statistic[0]['title'];
                }else{
                    $data['legend3'] = 'No data';
                }                
                // $data['xaxis3'] = '"2015-09-24","2015-07-30","2015-07-13","2015-06-24","2015-06-16"';
                // $data['serie_value3'] = '3,4,1,6,5';
                // $data['nb_total_viewers'] =  13;


                // echo '<pre>';print_r($like_statistic);echo '</pre>';
                // echo '<pre>';print_r($comment_statistic);echo '</pre>';
                // echo '<pre>';print_r($viewers_statistic);echo '</pre>';


            }
            
            $html['html']['content']  = $this->load->view('home/channel_statistic_by_app', $data, TRUE);
            $html['html']['header']   = $this->load->view('template/tpl_header', $html, TRUE);
            $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, TRUE);
            $this->load->view('template/tpl_one_column', $html, false);
        }  else {
            redirect('login', 'refresh');
        }
    }    

    public function ajax_content_list(){
        $result = 0;
        $msg = '';
        $selected_apps_id = !empty($_POST['selected_apps_id']) ? $_POST['selected_apps_id'] : '';
       
        if($selected_apps_id != ''){
            $contents = $this->home_lib->get_content_list($selected_apps_id);
            if($contents){
                $msg = '<select class="form-control" id="content_selector" name="s_content_id">';
                foreach($contents as $v){
                    $msg .= '<option value="'.$v['content_id'].'">'.$v['title'].'</option>';
                };
                $msg .= '</select>';
                $result = 1;
            }else{
                $result = 0;
                $msg = "Something's wrong !";
            }
        }else{
            $result = 0;
            $msg = "Something's wrong !";
        }

        $response = json_encode(['result'=>$result,'msg'=>$msg]);
        header('Content-Type: application/json');
        echo $response;
    }    

    public function ajax_channel_list(){
        $result = 0;
        $msg = '';
        $selected_apps_id = !empty($_POST['selected_apps_id']) ? $_POST['selected_apps_id'] : '';
       
        if($selected_apps_id != ''){
            $channels = $this->home_lib->get_channel_list($selected_apps_id);
            if($channels){
                $msg = '<select class="form-control" id="channel_selector" name="s_channel_id">';
                foreach($channels as $v){
                    $msg .= '<option value="'.$v['channel_id'].'">'.$v['title'].'</option>';
                };
                $msg .= '</select>';
                $result = 1;
            }else{
                $result = 0;
                $msg = "Something's wrong !";
            }
        }else{
            $result = 0;
            $msg = "Something's wrong !";
        }

        $response = json_encode(['result'=>$result,'msg'=>$msg]);
        header('Content-Type: application/json');
        echo $response;
    }    

}