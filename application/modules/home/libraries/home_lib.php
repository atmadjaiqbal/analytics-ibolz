<?php
class Home_Lib {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
    }

    function channel_statistic() {
        $textquery = "
			SELECT x.channel_id, x.channel_name, y.count_mobile_id, y.count_client_id, duration
			FROM
			(
				SELECT distinct ac.channel_id , ch.channel_name
				FROM  apps_channel ac
				inner join channel ch on ac.channel_id = ch.channel_id
				where ch.channel_type_id in (1,2)
			) x
			LEFT JOIN
			(
				SELECT channel_id, count(DISTINCT mobile_id) as count_mobile_id, count(client_id) count_client_id ,
					sum(TIMESTAMPDIFF(MINUTE, start_date, end_date)) duration
				FROM content_activity
				GROUP BY  channel_id
			) y on x.channel_id = y.channel_id
			ORDER BY x.channel_name
		";
        $query = $this->CI->db->query($textquery);
        return $query->result();
    }

    function get_apps_by_groups($role=null, $groupsid=null)
    {
        if($role != 'admin')
        {
            $sql = "SELECT g.`groups_id`, g.`groups_name`, ga.apps_id
                    FROM groups_apps ga INNER JOIN groups g
                    ON ga.`groups_id` = g.`groups_id`
                    WHERE ga.`groups_id` = '$groupsid'";
            $row = $this->CI->db->query($sql)->result_array();
        } else {
            $sql = "SELECT g.`groups_id`, g.`groups_name`, ga.apps_id
                    FROM groups_apps ga INNER JOIN groups g
                    ON ga.`groups_id` = g.`groups_id`";
            $row = $this->CI->db->query($sql)->result_array();
        }
        return $row;
    }

    function count_device_by_apps_installed($apps=[]) {
        $condition = !empty($apps) ? 'AND apps.apps_id IN ('.implode(',',$apps).')' : ''; 
        $textquery = "SELECT apps_mobile_id.apps_id, apps.app_name, COUNT(DISTINCT apps_mobile_id.mobile_id) total_device, COALESCE(device_online.total_device_online,0) as total_device_online, COALESCE(customer.total_customer,0) as total_customer, COALESCE(online.total_customer_online,0) as total_customer_online
                                FROM apps_mobile_id
                                LEFT JOIN apps ON apps_mobile_id.apps_id = apps.apps_id
                                LEFT JOIN (
                                  SELECT apps_id, COALESCE(COUNT(DISTINCT customer_id),0) AS total_customer_online
                                     FROM apps_activity
                                     WHERE DATE(activity_date) = CURDATE()
                                     AND apps_id != ''
                                     AND mobile_id != ''
                                     AND customer_id != ''
                                     AND activity_id = 'auth'
                                     GROUP BY apps_id
                                ) online ON apps_mobile_id.apps_id = online.apps_id
                                LEFT JOIN (
                                  SELECT apps_id, COALESCE(COUNT(DISTINCT customer_id),0) AS total_customer
                                     FROM customer
                                     WHERE apps_id != ''
                                     AND mobile_id != ''
                                     AND customer_id != ''
                                     GROUP BY apps_id
                                ) customer ON apps_mobile_id.apps_id = customer.apps_id
                                LEFT JOIN (
                                  SELECT apps_id, COALESCE(COUNT(DISTINCT mobile_id),0) AS total_device_online
                                     FROM apps_activity
                                     WHERE DATE(activity_date) = CURDATE()
                                     AND apps_id != ''
                                     AND activity_id = 'auth'
                                     AND mobile_id != ''
                                     AND mobile_id != 'webversion'
                                     GROUP BY apps_id
                                ) device_online ON apps_mobile_id.apps_id = device_online.apps_id
                                WHERE apps_mobile_id.apps_id != ''
                                AND apps_mobile_id.mobile_id != ''
                                AND apps.app_name != ''
                                ".$condition."GROUP BY apps_mobile_id.apps_id";
        $query = $this->CI->db->query($textquery);
        return $query->result_array();
    }


    function get_apps(){
        $sql = "SELECT * FROM apps";
        $query = $this->CI->db->query($sql);
        return $query->result_array();
    }

    function get_content_list($apps_id){
        // Finding contents based on apps_id
        $sql = "SELECT 
                m.menu_id, p.apps_id, p.product_id,'' channel_category_id, '' channel_id, '' channel_type_id, c.content_id, 
                IF(c.status = '1','channel','content') content_type,
                c.title, cc.category_name, video_thumbnail thumbnail,  description, prod_year, video_length video_duration,video_length,
                (SELECT count(content_id) FROM content_like WHERE content_id=c.content_id AND ulike=1) as countlike,    
                (SELECT count(content_id) FROM content_comment WHERE content_id=c.content_id AND status=1) as countcomment,                 
                countviewer,d.icon_size, d.poster_size, p.price
            FROM 
                menu_product p
            INNER JOIN menu m on p.menu_id = m.menu_id
            INNER JOIN content c on p.product_id = c.content_id and c.active='1'
            INNER JOIN apps d on p.apps_id = d.apps_id
            INNER JOIN content_category cc ON c.content_category_id = cc.`content_category_id`
            WHERE c.active='1' AND p.apps_id = '".$apps_id."' AND m.apps_id = '".$apps_id."'";
        $query = $this->CI->db->query($sql);
        return $query->result_array();
    }

    function get_channel_list($apps_id){
        // Finding channels based on apps_id
        $sql = "SELECT '' channel_category_id, c.channel_id, c.channel_type_id,
            c.channel_id content_id, 'channel' content_type, alias title, c.thumbnail,
            SUBSTR(channel_descr, 0, 100) description, YEAR(CURDATE()) prod_year, '86400' video_duration
        FROM
            menu_product p
        INNER JOIN menu m ON p.menu_id = m.menu_id 
        INNER JOIN channel c ON p.product_id = c.channel_id 
        WHERE c.status ='1' AND p.apps_id = '".$apps_id."'";
        $query = $this->CI->db->query($sql);
        return $query->result_array();
    }

    function get_like_range_by_content_id($content_id,$start,$end){
        $sql = "SELECT  cl.content_id,co.title, COUNT(*) nb_like, DATE(cl.created) specific_date
            FROM content_like cl
            INNER JOIN content co ON co.`content_id` = cl.`content_id`
            LEFT JOIN customer c ON cl.customer_id = c.customer_id
            WHERE cl.content_id = '".$content_id."' AND ulike ='1'
            AND DATE(cl.created) >= '".$start."' AND DATE(cl.created) <= '".$end."'
            GROUP BY DATE(cl.created) ORDER BY cl.created DESC";
        $query = $this->CI->db->query($sql);
        return $query->result_array();
    }

    function get_comments_by_content_id($content_id,$start,$end){
        $sql = "SELECT  COUNT(*) nb_comment, cc.content_id, DATE(cc.created) created,co.title
        FROM content_comment cc 
        INNER JOIN content co ON co.`content_id` = cc.`content_id`
        LEFT JOIN customer c ON cc.customer_id = c.customer_id AND c.apps_id ='com.balepoint.ibolz.bandung'
        WHERE cc.content_id = '".$content_id."' AND DATE(cc.created) >= '".$start."' AND DATE(cc.created) <= '".$end."'
        GROUP BY DATE(cc.created) ORDER BY DATE(cc.created) DESC";
        $query = $this->CI->db->query($sql);
        return $query->result_array();

    }

    function get_viewers_by_content_id($condition='',$apps_id,$start,$end){
        $sql = "SELECT ca.`apps_id`,ca.`content_id`,ca.`channel_id`,ca.`customer_id`,
        ca.`content_title`,COUNT(*) nb,DATE(start_date) created,co.title  
        FROM content_activity ca
        INNER JOIN content co ON co.`content_id` = ca.`content_id`
        WHERE 
        DATE(ca.start_date) >= '".$start."' AND DATE(ca.start_date) <= '".$end."'
        AND apps_id='".$apps_id."' ";
        if($condition != ''){
            $sql .= $condition;
        }
        
        $sql .= " GROUP BY DATE(ca.start_date) ORDER BY DATE(ca.start_date) ASC";            
        $query = $this->CI->db->query($sql);
        return $query->result_array();
    }

    function get_viewers_by_content_id2($condition='',$apps_id,$start,$end){
        $sql = "SELECT ca.`apps_id`,ca.`content_id`,ca.`channel_id`,ca.`customer_id`,
        cc.`alias` title,COUNT(*) nb,DATE(start_date) created  
        FROM content_activity ca
        INNER JOIN content co ON co.`content_id` = ca.`content_id`
        INNER JOIN channel cc ON(cc.channel_id = ca.channel_id)
        WHERE 
        DATE(ca.start_date) >= '".$start."' AND DATE(ca.start_date) <= '".$end."'
        AND apps_id='".$apps_id."' ";
        if($condition != ''){
            $sql .= $condition;
        }
        
        $sql .= " GROUP BY DATE(ca.start_date) ORDER BY DATE(ca.start_date) ASC";      
        $query = $this->CI->db->query($sql);
        return $query->result_array();
    }

}