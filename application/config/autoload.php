<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array(APPPATH.'third_party/codeigniter-forensics');
$autoload['libraries'] = array('rest', 'session','pagination', 'curl','database');
$autoload['drivers'] = array();
$autoload['helper'] = array('url', 'file');
$autoload['config'] = array('config');
$autoload['language'] = array();
$autoload['model'] = array();
