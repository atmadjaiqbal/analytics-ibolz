<div class="container">
   <footer id="footer" style="margin-top:20px;">
       <p align="center"><strong>&copy; <?php echo date('Y'); ?> IBOLZ Indonesia</strong></p>
   </footer>
</div>
</body>

<script type="text/javascript">
    <?php $setting = array(
        'base_url' => base_url()
    ); ?>
    var settings = <?php echo json_encode($setting); ?>;
</script>

<script src="<?php echo $this->config->base_url(); ?>assets/js/ibolz.global.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

    })
</script>

</html>
