<!DOCTYPE html>
<html lang="en-US" dir="ltr" class="no-js">
<head>
    <?php // echo $html['title'] ?>
    <meta name="description" content="admin tools ibolz Indonesia" />
    <meta charset="utf-8" />
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta name="author" content="Ibolz Team" />
    <title><?php echo !empty($title) ? $title : 'iBolz Indonesia'; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/ibolz.analytics.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css"/>

    <script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->base_url('assets/js/highcharts.js'); ?>"></script>
    <script src="<?php echo $this->config->base_url('assets/js/exporting.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->config->base_url('assets/js/moment.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->config->base_url('assets/js/bootstrap-datetimepicker.min.js'); ?>"></script>
    <script src="<?php echo $this->config->base_url('assets/js/custom.js'); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            /*
            $('#language').live('change', function(event) {
                var language = $('#language').val();
                var url = '<?php echo base_url() ?>login/language?language='+language;
                $.getJSON(url,{},function(data)
                {
                    location.reload();
                });
            });
            */

        });
    </script>

</head>
<body>

<div id="wrapper" class="clearfix">
    <header id="header">
        <h1><span class="title">IBOLZ INDONESIA</span></h1>
        <div class="admin-info">
            <h2>Analytic & Monitoring Panel</h2>
            <p style="font-size: 14px; font-weight: 800;margin-top:-3px;"><?php echo $this->session->userdata['logged_in']['account']->groups->groups_name;?></p>
        </div>
        <p></p>
        <p class="date"><?php  echo date('l, d F Y H:i:s');?><br/><?php echo $this->session->userdata['logged_in']['username'];?></p>
    </header>

    <nav id="account-nav">
        <ul>
            <li class="acc-account has-subnav"><a href="#">Account</a></li>
            <li class="acc-settings"><a href="#">Settings</a></li>
            <li class="acc-help"><a href="#">Help &amp; FAQ</a></li>
            <li class="acc-support"><a href="#">Support</a></li>
            <li class="acc-logout"><a href="<?php echo base_url(); ?>login/logout">Logout</a></li>
        </ul>
    </nav>

    <nav id="main-nav">
        <!-- ul>
            <li class="has-subnav dashboard"><a href="#">Dashboard</a>
                <ul>
                    <li class="dashboard-statistik"><a href="<?php echo base_url(); ?>home/channel_statistik">Channel Statistik</a></li>
                    <li class="dashboard-graph"><a href="<?php echo base_url(); ?>home/graph">Graph</a></li>
                    <li class="dashboard-content"><a href="#">Content Info</a></li>
                </ul>
            </li>
            <li class="has-subnav menuchart"><a href="#">Chart</a>
                <ul>
                    <li class="contentviewer"><a href="<?php echo base_url(); ?>product/product_categories">Content Viewers</a></li>
                    <li class="channelviewer"><a href="<?php echo base_url(); ?>product/product">Channel Viewers</a></li>
                </ul>
            </li>
        </ul -->

        <ul class="nav" role="navigation">
            <li class="dropdown">
                <a id="drop1" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Statistik<b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>home/statistik_channel">Channel Statistik</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>home/statistik_content">Content Statistik</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>home/statistik_content_by_app">Statistics by Content</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>home/statistik_channel_by_app">Statistics by Channels</a></li>

                </ul>
            </li>

            <li class="dropdown">
                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Channel Monitoring<b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>monitoring/cctv_ntmc">NTMC CCTV</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>monitoring/fta">FTA</a></li>
                </ul>
            </li>
        </ul>

    </nav>
</div>
